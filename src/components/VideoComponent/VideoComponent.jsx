import React from 'react';
import Card from '@mui/material/Card';
import CardActionArea from '@mui/material/CardActionArea';
import CardMedia from '@mui/material/CardMedia';
import PropTypes from 'prop-types';

const videos = {
  1:"https://videoguide.mytechpubs.com/videos/01-OpenACMM.mp4",
  2:"https://videoguide.mytechpubs.com/videos/02-OpenACMMinBrowser.mp4",
  3:"https://videoguide.mytechpubs.com/videos/03-SendBrowserUXByEmail.mp4",
  4:"https://videoguide.mytechpubs.com/videos/04-OpenLocalCMMinBrowser.mp4",
  5:"https://videoguide.mytechpubs.com/videos/05-DownloadPlugin.mp4",
  6:"https://videoguide.mytechpubs.com/videos/06-myCMMfilter.mp4",
  7:"https://videoguide.mytechpubs.com/videos/07-Request.mp4",
  8:"",
  9:"",
}

// eslint-disable-next-line
export default class extends React.Component {
  static propTypes = {
    props: PropTypes.any,
  };
  state = {
    screenWidth: window.innerWidth,
    linkVideo: "",
  }
  componentDidMount() {
    this.setState({linkVideo: videos[this.props.props]})
  }
  render() {
    return (
      <div>
        <Card>
          <CardActionArea>
            <CardMedia style={{ width: 1280, height: 720 }}>
                <video
                  width="1280"
                  height="720"
                  controls
                  controlsList="nodownload"
                  style={{ marginTop: '20px' }}
                >
                  <source src={this.state.linkVideo} type="video/mp4" />
                </video>
            </CardMedia>
          </CardActionArea>
        </Card>
      </div>
    );
  }
}