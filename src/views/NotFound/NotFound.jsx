import React from 'react';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';

const NotFound = () => {

  return (
    <div>
      <Grid
        container
        justify="center"
        spacing={4}
      >
        <Grid
          item
        >
          <div>
            <Typography variant="h1">
              404: The page you are looking for isn’t here
            </Typography>
            <Typography variant="subtitle2">
              You either tried some shady route or you came here by mistake.
              Whichever it is, try using the navigation
            </Typography>
            <img
              alt="Under development"
              src="/images/undraw_page_not_found_su7k.svg"
            />
          </div>
        </Grid>
      </Grid>
    </div>
  );
};

export default NotFound;
