import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import useToken from './useToken';

import { MainLayout, Minimal } from './layouts'
import {
  Login as LoginView,
  Dashboard as DashboardView,
  NotFound as NotfoundView,
} from './views'

function PublicRoute({ children, isAuthenticated, layout, ...rest }) {
  return (
    <Route
      {...rest}
      render={
        ({ location }) => (
          !isAuthenticated ? (
            children
          ) : (
            <Redirect
              to={{
                pathname: '/dashboard',
                state: { from: location }
              }}
            />
          ))
      }
    />
  );
}

function PrivateRoute({ children, isAuthenticated, ...rest }) {
  return (
    <Route
      {...rest}
      render={
        ({ location }) => (
          isAuthenticated
            ? (
              children
            ) : (
              <Redirect
                to={{
                  pathname: '/login',
                  state: { from: location }
                }}
              />
            ))
      }
    />
  );
}




const Routes = () => {
  const { token, setToken } = useToken();
  const isAuthenticated = token
  return (
    <div style={{ height: '100%' }}>
      <Switch>
      <Redirect
          exact
          from="/"
          to="/dashboard"
        />
        <PublicRoute
          path="/login"
          isAuthenticated={isAuthenticated}
        >
          <Minimal />
          <LoginView setToken={setToken} />
        </PublicRoute>
        <PublicRoute
          path="/not-found"
        >
          <Minimal />
          <NotfoundView />
        </PublicRoute>
        <PrivateRoute
          path="/dashboard"
          isAuthenticated={isAuthenticated}
        >
          <MainLayout />
          <DashboardView />
        </PrivateRoute>
        <Redirect to="/not-found" />
      </Switch>
    </div>
  )
}
export default Routes;