import React from 'react';
import './App.css';
import { Router } from 'react-router-dom';
import Routes from './Routes';
import { createBrowserHistory } from 'history';

import { ThemeProvider } from '@mui/material/styles';
import theme from './theme';

const browserHistory = createBrowserHistory({ forceRefresh: true });

const App = () => {
  return (
    <div className="App">
      <ThemeProvider theme={theme}>
        <Router history={browserHistory}>
          <Routes />
        </Router>
        </ThemeProvider>
    </div>
  );
};

export default App;
