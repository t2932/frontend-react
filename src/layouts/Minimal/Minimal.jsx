import React from 'react';
import { Navbar } from './components';
import PropTypes from 'prop-types';


const Minimal = () => {

  return (
    <div>
      <Navbar />
    </div>
  )
}

Minimal.propTypes = {
  children: PropTypes.node
};

export default Minimal;