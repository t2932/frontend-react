import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles((theme) => ({
  root: {
    boxShadow: 'none',
  },
  flexGrow: {
    flexGrow: 1
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
  backgroundColor: theme.palette.background.default,
}));

export default function Navbar() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <AppBar position="static" style={{ backgroundColor: '#1a237e' }}>
        <Toolbar>
          {/* <img
          style={{width: '200px'}}
            alt="Logo"
            src="images/SiteWebHQ.jpg"
          /> */}
          <Typography variant="h6" className={classes.title}>
            Template React
          </Typography>
        </Toolbar>
      </AppBar>
    </div>
  );
}