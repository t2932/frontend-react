import React from 'react';
import { Drawer, Divider, IconButton }
  from '@material-ui/core';
import { List, ListItem, ListItemIcon, ListItemText }
  from '@material-ui/core';
import Home from
  '@material-ui/icons/Home';
import ReorderIcon from '@material-ui/icons/Reorder';
import AccountCircleIcon from
  '@material-ui/icons/AccountCircle';
import { Link } from 'react-router-dom';

const styles = {
  sideNav: {
    marginTop: '-60px',
    zIndex: 3,
    marginLeft: '0px',
    position: 'fixed',
  },
  link: {
    color: 'black',
    textDecoration: 'none',
  },
  root: {},
  item: {
    display: 'flex',
    paddingTop: 0,
    paddingBottom: 0
  },
  button: {
    padding: '10px 8px',
    justifyContent: 'flex-start',
    textTransform: 'none',
    letterSpacing: 0,
    width: '100%',
  },
  icon: {
    width: 24,
    height: 24,
    display: 'flex',
    alignItems: 'center',
  },
};

export default class MarerialUIDrawer
  extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isDrawerOpened: false,
    };
  }
  toggleDrawerStatus = () => {
    this.setState({
      isDrawerOpened: true,
    })
  }
  closeDrawer = () => {
    this.setState({
      isDrawerOpened: false,
    })
  }
  render() {
    const { isDrawerOpened } = this.state;
    return (
      <div>
        <div style={styles.sideNav}>
          <IconButton onClick={this.toggleDrawerStatus}>
            {!isDrawerOpened ? <ReorderIcon /> : null}
          </IconButton>
        </div>
        <Divider />
        <Drawer
          variant="permanent"
          open={isDrawerOpened}
          onClose={this.closeDrawer}
        >
          <Link to='/home' style={styles.link}>
            <List>
              <ListItem button key='Home'>
                <ListItemIcon><Home />
                </ListItemIcon>
                <ListItemText primary='Home' />
              </ListItem>
            </List>
          </Link>
          <Link to='/account' style={styles.link}>
            <List>
              <ListItem button key='Account'>
                <ListItemIcon><AccountCircleIcon />
                </ListItemIcon>
                <ListItemText primary='Account' />
              </ListItem>
            </List>
          </Link>
        </Drawer>
      </div>
    );
  }
}
