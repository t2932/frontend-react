import React from 'react';
import { Navbar, Sidebar } from './components';
import PropTypes from 'prop-types';


const MainLayout = () => {

  return (
    <div>
      <Navbar />
      <Sidebar />
    </div>
  )
}

MainLayout.propTypes = {
  children: PropTypes.node
};

export default MainLayout;