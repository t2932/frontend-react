# Template front React
Just a simple frontent template that includes login view (fonctionnal) to work with the Django API, and a home view to edit as you wish.

## Setup
Simply follow this commands to setup dev environment :

```bash
# clone the repo
git clone git@gitlab.com:t2932/frontend-react.git

# install dependancies
npm i

# launch dev server
npm run dev
```